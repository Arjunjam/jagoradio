package modal


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Video(
        @SerializedName("category_id")
        val categoryId: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("image")
        val image: String?,
        @SerializedName("title")
        val title: String?,
        @SerializedName("url")
        val url: String?,
        @SerializedName("video_name")
        val videoName: String?
) : Parcelable,Row() {
    override fun data() {

    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(categoryId)
        writeString(id)
        writeString(image)
        writeString(title)
        writeString(url)
        writeString(videoName)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Video> = object : Parcelable.Creator<Video> {
            override fun createFromParcel(source: Parcel): Video = Video(source)
            override fun newArray(size: Int): Array<Video?> = arrayOfNulls(size)
        }
    }
}