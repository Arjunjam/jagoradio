package modal


import com.google.gson.annotations.SerializedName

data class VideoCategoryResponse(
        @SerializedName("data")
    val `videoCategories`: List<VideoCategory>,
        @SerializedName("message_text")
    val messageText: String,
        @SerializedName("success")
    val success: Int
)