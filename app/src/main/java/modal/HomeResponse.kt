package modal


import com.google.gson.annotations.SerializedName

data class HomeResponse(
        @SerializedName("data")
    val homeData: HomeData,
        @SerializedName("message_text")
    val messageText: String,
        @SerializedName("success")
    val success: Int
)