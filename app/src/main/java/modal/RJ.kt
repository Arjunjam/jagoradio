package modal


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RJ(
        @SerializedName("id")
        val id: String?,
        @SerializedName("image")
        val image: String?,
        @SerializedName("rj_name")
        val rjName: String?,
        @SerializedName("description")
        val description: String?
) : Parcelable,Row() {
    override fun data() {

    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(image)
        parcel.writeString(rjName)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RJ> {
        override fun createFromParcel(parcel: Parcel): RJ {
            return RJ(parcel)
        }

        override fun newArray(size: Int): Array<RJ?> {
            return arrayOfNulls(size)
        }
    }
}