package modal


import com.google.gson.annotations.SerializedName

data class Program(
    @SerializedName("datetime")
    val datetime: String,
    @SerializedName("endtime")
    val endtime: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("program_name")
    val programName: String,
    @SerializedName("rj_id")
    val rjId: String,
    @SerializedName("starttime")
    val starttime: String,
    @SerializedName("title")
    val title: String
)