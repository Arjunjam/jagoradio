package modal


import com.google.gson.annotations.SerializedName

data class HomeData(
    @SerializedName("rjs")
    val rjs: List<RJ>,
    @SerializedName("videos")
    val videos: List<Video>
)