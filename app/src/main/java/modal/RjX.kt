package modal


import com.google.gson.annotations.SerializedName

data class RjX(
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("rj_name")
    val rjName: String
)