package modal


import com.google.gson.annotations.SerializedName

data class ProgramResponse(
        @SerializedName("data")
    val `programList`: List<Program>,
        @SerializedName("message_text")
    val messageText: String,
        @SerializedName("success")
    val success: Int
)