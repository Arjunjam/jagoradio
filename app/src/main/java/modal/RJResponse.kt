package modal


import com.google.gson.annotations.SerializedName

data class RJResponse(
        @SerializedName("data")
    val `rjList`: List<RJ>,
        @SerializedName("message_text")
    val messageText: String,
        @SerializedName("success")
    val success: Int
)