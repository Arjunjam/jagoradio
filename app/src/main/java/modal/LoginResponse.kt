package modal


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("data")
    val `data`: Int,
    @SerializedName("message_text")
    val messageText: String,
    @SerializedName("success")
    val success: Int
)