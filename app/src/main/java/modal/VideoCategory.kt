package modal


import com.google.gson.annotations.SerializedName

data class VideoCategory(
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("id")
    val id: String
)