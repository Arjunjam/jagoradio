package modal

import com.google.gson.annotations.SerializedName

data class RJID(
        @SerializedName("rj_id")
        val id:String
)
