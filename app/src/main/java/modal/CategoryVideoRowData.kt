package modal

data class CategoryVideoRowData(
        val category: VideoCategory,
        val videos:List<Video>)