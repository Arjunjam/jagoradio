package modal


import com.google.gson.annotations.SerializedName

data class VideoResponse(
        @SerializedName("data")
    val `videos`: List<Video>,
        @SerializedName("message_text")
    val messageText: String,
        @SerializedName("success")
    val success: Int
)