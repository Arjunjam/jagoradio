package com.jago.app

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.jago.app.databinding.FragmentSplashBinding
import com.jago.app.util.SaveToSharedPrefences
import java.util.*

class SplashFragment :Fragment(){

    companion object {
        fun newInstance() = SplashFragment()

    }

    private lateinit var splashBinding:FragmentSplashBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        splashBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_splash,container,false)
        (activity as MainActivity).getSupportActionBar()?.hide()
        splashTimer()
        return splashBinding.root
    }
    private fun splashTimer() {
       val timer :Timer = Timer()
        val handler = Handler()
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Runnable {
                    if(activity?.let { SaveToSharedPrefences.isTokenSaved(it) }!!){
                        openHomeActivity()
                    }else{
                        openLoginFragment()
                    }

                     })
            }
        }, 2000)
    }

    private fun openLoginFragment() {
        activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, LoginFragment.Companion.newInstance())
                ?.commitNow();

    }

    private fun openHomeActivity() {
        val intent = Intent(activity, HomeActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

}