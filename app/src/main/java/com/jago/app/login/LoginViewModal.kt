package com.jago.app.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jago.app.network.APIFactory
import com.jago.app.repositories.LoginRepo
import com.jago.app.repositories.RJRepo
import com.jago.app.util.InternetConnectivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import modal.LoginResponse
import modal.Program
import modal.RJ
import kotlin.coroutines.CoroutineContext

class LoginViewModal(application: Application): AndroidViewModel(application) {


    private val parentJob = Job()
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)
    private val loginRepo : LoginRepo = LoginRepo(APIFactory.postLoginApi())

    var liveDataLogin:MutableLiveData<LoginResponse> =MutableLiveData()



    fun  postLogin(email: String, profile: String, firstName: String?, lastName: String?, image: String?,
                   platform: String){
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
                val loginResponse:LoginResponse?=loginRepo.postLogin(email, profile, firstName,
                        lastName, image, platform)
                liveDataLogin.postValue(loginResponse)

            }
        }
    }
}