package com.jago.app.video

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jago.app.HomeActivity
import com.jago.app.R
import com.jago.app.databinding.FragmentVideoBinding
import com.jago.app.mediaPlayer.LocalPlayerActivity
import modal.CategoryVideoRowData
import modal.Video


class VideoFragment : Fragment() ,CategoryVideoListAdapter.ItemClickListener {

    private lateinit var videoViewModel: VideoViewModel

    private lateinit var fragmentVideoBinding: FragmentVideoBinding
    private lateinit  var linearLayout: LinearLayout
    private lateinit var  recyclerView:RecyclerView
    private lateinit var mEmptyView: View
    private lateinit var mLoadingView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentVideoBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_video,container,false)
//        videoViewModel =
//                ViewModelProviders.of(this).get(VideoViewModel::class.java)

        linearLayout = fragmentVideoBinding.linearLayoutVideo
        recyclerView = fragmentVideoBinding.recyclerView
        mLoadingView = fragmentVideoBinding.progressIndicator
        mEmptyView = fragmentVideoBinding.emptyView
        val toolbar =  (activity as HomeActivity)!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.findViewById<ImageView>(R.id.toolbarImage).visibility=View.GONE
        return fragmentVideoBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        videoViewModel =
                activity?.let { ViewModelProviders.of(it).get(VideoViewModel::class.java) }!!

        videoViewModel.getVideos()
        activity?.let {
            videoViewModel.videoLiveData.observe(viewLifecycleOwner, Observer {
            })
        }

        // videoViewModel.getVideoCategory()
        videoViewModel.categoryVideoRowDataList.observe(viewLifecycleOwner, Observer {
            setUpRecyclerViewCategoryVideo(it)
        })

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    private fun setUpRecyclerViewCategoryVideo(categoryVideoRowData: List<CategoryVideoRowData>) {
        val videoRecyclerViewAdapter = activity?.let { CategoryVideoListAdapter(this, activity) }
        recyclerView.adapter = videoRecyclerViewAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        videoRecyclerViewAdapter?.setData(categoryVideoRowData)
        mLoadingView.visibility = View.GONE
        mEmptyView.visibility = if (null == categoryVideoRowData || categoryVideoRowData.isEmpty()) View.VISIBLE else View.GONE
    }


    override fun itemClicked(v: View?, item: CategoryVideoRowData?, position: Int) {
        (activity as HomeActivity?)?.stopRadio()
        val intent = Intent(activity, LocalPlayerActivity::class.java)
         val video: Video  = v?.getTag() as Video
        intent.putExtra("media",video)
        intent.putExtra("shouldStart", false)
        ActivityCompat.startActivity(requireActivity(), intent, null)
    }

    override fun openNextListFragment(id:String,name:String,position: Int) {
        Navigation.createNavigateOnClickListener(R.id.action_fragment_video_list)
        val bundle:Bundle= Bundle()
        bundle.apply {
            putString("categoryId",id)
            putString("categoryName",name)
        }
        Navigation.findNavController(view!!).navigate(R.id.action_fragment_video_list,bundle)
    }
}
