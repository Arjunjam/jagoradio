package com.jago.app.video

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jago.app.HomeActivity
import com.jago.app.R
import com.jago.app.databinding.FragmentVideoListBinding
import com.jago.app.mediaPlayer.LocalPlayerActivity
import modal.Video

class VideoListFragment :Fragment(),VideoListAdapter.ItemClickListener{


    companion object {
        fun newInstance() = VideoListFragment()

    }
    private lateinit var categoryId:String
    private lateinit var videoViewModel: VideoViewModel
    private lateinit var fragmentVideoListBinding: FragmentVideoListBinding
    private lateinit var  recyclerView: RecyclerView
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        fragmentVideoListBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_video_list,container,false)
//        videoViewModel =
//                ViewModelProviders.of(this).get(VideoViewModel::class.java)


        recyclerView = fragmentVideoListBinding.recyclerViewVideoList
        val args = arguments
         categoryId = args?.getString("categoryId", "1").toString()
        val categoryName = args?.getString("categoryName", "Videos").toString()

        (activity as HomeActivity).getSupportActionBar()?.setTitle(categoryName)
        val toolbar =  (activity as HomeActivity)!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.findViewById<ImageView>(R.id.toolbarImage).visibility=View.GONE
        return fragmentVideoListBinding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        videoViewModel =
                activity?.let { ViewModelProviders.of(it).get(VideoViewModel::class.java) }!!
        videoViewModel.getVideoCategory(categoryId)
        activity?.let {
            videoViewModel.videoLiveData.observe(viewLifecycleOwner, Observer {
                setUpVideoRecyclerView(it)
            })
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



    }
    private fun setUpVideoRecyclerView(video: List<Video>) {
        val videoRecyclerViewAdapter = activity?.let { VideoListAdapter(this, activity) }
        recyclerView.adapter = videoRecyclerViewAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        videoRecyclerViewAdapter?.setData(video)
    }

    override fun itemClicked(v: View?, item: Video?, position: Int) {
        val transitionName = getString(R.string.transition_image)
        val viewHolder = recyclerView.findViewHolderForPosition(position) as VideoListAdapter.ViewHolder
        val imagePair = Pair
                .create(viewHolder.imageView as View, transitionName)
        val options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(requireActivity(), imagePair)
        (activity as HomeActivity?)?.stopRadio()
        val intent = Intent(activity, LocalPlayerActivity::class.java)
        intent.putExtra("media",item)
        intent.putExtra("shouldStart", false)
        ActivityCompat.startActivity(requireActivity(), intent, options.toBundle())
    }
}