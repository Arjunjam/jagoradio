package com.jago.app.video

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jago.app.network.APIFactory
import com.jago.app.repositories.VideoRepo
import com.jago.app.util.InternetConnectivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import modal.CategoryVideoRowData
import modal.Video
import kotlin.coroutines.CoroutineContext

class VideoViewModel(application: Application) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Video Fragment"
    }
    val text: LiveData<String> = _text


    private val parentJob = Job()
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)
    private val videoRepository : VideoRepo = VideoRepo(APIFactory.getVideoApi())
    val videoLiveData = MutableLiveData<MutableList<Video>>()
    private  var categoryVideoRowDataArrayList:MutableList<CategoryVideoRowData> = ArrayList()
     var  categoryVideoRowDataList :MutableLiveData<MutableList<CategoryVideoRowData>> = MutableLiveData<MutableList<CategoryVideoRowData>>()
    private lateinit var videos:MutableList<Video>
    fun getVideos() {
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
                videos= videoRepository.getVideo()!!
                //videoLiveData.postValue(videos)
                //categoryVideoRowDataList.
                getVideoCategory()
            }
        }
    }
    fun getVideoCategory() {
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
                categoryVideoRowDataList.apply {
                    categoryVideoRowDataArrayList.clear()
                }
                val videoCategories = videoRepository.getVideoCategory()
                   for(videoCategory in videoCategories!!){
                       var videos: List<Video> = videos.filter { it.categoryId ==videoCategory.id}
                        var categoryVideoRowData = CategoryVideoRowData(videoCategory,videos)
                        categoryVideoRowDataArrayList.add(categoryVideoRowData)
                   }

                    categoryVideoRowDataList.postValue(categoryVideoRowDataArrayList)
            }
        }
    }

    fun getVideoCategory(categoryId:String) {
            scope.launch {
                    var videos: MutableList<Video> = videos.filter { it.categoryId ==categoryId} as MutableList<Video>
                    videoLiveData.postValue(videos)
            }
        }

}