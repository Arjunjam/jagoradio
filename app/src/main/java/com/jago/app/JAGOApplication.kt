package com.jago.app

import android.app.Application
import android.content.Context

class JAGOApplication  :Application(){

    companion object{
        lateinit var instance:JAGOApplication

        fun getInstance():Context{
            return instance
        }
    }
    init {
        instance =this
    }
}