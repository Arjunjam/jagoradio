package com.jago.app.network

import kotlinx.coroutines.Deferred
import modal.Video
import modal.VideoCategory
import modal.VideoCategoryResponse
import modal.VideoResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface VideoServiceApi {

    @GET("video_url")
    fun fetchVideoAsync()  : Deferred<Response<VideoResponse>>

    @GET("category")
    fun fetchVideoCategory()  : Deferred<Response<VideoCategoryResponse>>
}