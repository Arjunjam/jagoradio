package com.jago.app.network

import kotlinx.coroutines.Deferred
import modal.ProgramResponse
import modal.RJID
import retrofit2.Response
import retrofit2.http.*

interface ProgramServiceAPI {
    @FormUrlEncoded
    @POST("program_api")
    fun fetchProgram(@Field("rj_id")rj_id: String)  : Deferred<Response<ProgramResponse>>
}