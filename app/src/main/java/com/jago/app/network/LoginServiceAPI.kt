package com.jago.app.network

import kotlinx.coroutines.Deferred
import modal.LoginResponse
import modal.ProgramResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginServiceAPI {
    @FormUrlEncoded
    @POST("new_user")
    fun postLogin(@Field("email")email: String,
                     @Field("profile_id")profile_id:String,
                     @Field("first_name")first_name:String?,
                     @Field("last_name")last_name:String?,
                     @Field("image_url")image_url:String?,
                     @Field("platform")platform:String)  : Deferred<Response<LoginResponse>>
}