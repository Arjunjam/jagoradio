package com.jago.app.network

import kotlinx.coroutines.Deferred
import modal.ProgramResponse
import modal.RJResponse
import modal.VideoResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RJServiceApi {
    @GET("rj_api")
    fun fetchRJAsync()  : Deferred<Response<RJResponse>>

}