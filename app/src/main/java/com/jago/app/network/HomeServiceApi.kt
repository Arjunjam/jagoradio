package com.jago.app.network

import kotlinx.coroutines.Deferred
import modal.HomeResponse
import modal.VideoResponse
import retrofit2.Response
import retrofit2.http.GET

interface HomeServiceApi {
    @GET("top4category_rj")
    fun fetchHomeAsync()  : Deferred<Response<HomeResponse>>
}