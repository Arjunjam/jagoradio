package com.jago.app.network

import android.util.Log
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jago.app.BuildConfig
import com.jago.app.R
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIFactory {
    const val API_TIMEOUT: Long = 30
    fun getVideoApi(): VideoServiceApi {
        val okHttpClient = makeOkHttpClient(
                makeLoggingInterceptor()
        )
        return makeVideoApi(okHttpClient, makeGson())
    }
    fun getRJApi(): RJServiceApi {
        val okHttpClient = makeOkHttpClient(
                makeLoggingInterceptor()
        )
        return makeRJApi(okHttpClient, makeGson())
    }

    fun getProgramApi(): ProgramServiceAPI {
        val okHttpClient = makeOkHttpClient(
                makeLoggingInterceptor()
        )
        return makeProgramApi(okHttpClient, makeGson())
    }

    fun getHomeApi(): HomeServiceApi {
        val okHttpClient = makeOkHttpClient(
                makeLoggingInterceptor()
        )
        return makeHomeApi(okHttpClient, makeGson())
    }

    fun postLoginApi(): LoginServiceAPI {
        val okHttpClient = makeOkHttpClient(
                makeLoggingInterceptor()
        )
        return makeLoginApi(okHttpClient, makeGson())
    }

    private fun makeOkHttpClient(
            httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(API_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(API_TIMEOUT, TimeUnit.SECONDS)
                .build()
    }

    private fun makeVideoApi(okHttpClient: OkHttpClient, gson: Gson): VideoServiceApi {
        return makeRetrofit(okHttpClient, gson).create(VideoServiceApi::class.java)
    }
    private fun makeRJApi(okHttpClient: OkHttpClient, gson: Gson): RJServiceApi {
        return makeRetrofit(okHttpClient, gson).create(RJServiceApi::class.java)
    }
    private fun makeProgramApi(okHttpClient: OkHttpClient, gson: Gson): ProgramServiceAPI {
        return makeRetrofit(okHttpClient, gson).create(ProgramServiceAPI::class.java)
    }
    private fun makeHomeApi(okHttpClient: OkHttpClient, gson: Gson): HomeServiceApi {
        return makeRetrofit(okHttpClient, gson).create(HomeServiceApi::class.java)
    }
    private fun makeLoginApi(okHttpClient: OkHttpClient, gson: Gson): LoginServiceAPI {
        return makeRetrofit(okHttpClient, gson).create(LoginServiceAPI::class.java)
    }
    private fun makeRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://www.appdeft.biz/appbackend/Api/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
    }

    private fun makeGson(): Gson {
        return GsonBuilder()
                .setLenient()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create()
    }

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor(httpLogger)
        loggingInterceptor.level =
                HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    private val httpLogger: HttpLoggingInterceptor.Logger by lazy {
        object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Log.println(
                        Log.VERBOSE,
                        R.string.web_service.toString(),
                        R.string.http_response.toString() + message
                )
            }
        }
    }
}