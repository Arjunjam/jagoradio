package com.jago.app.rj_program;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jago.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import modal.CategoryVideoRowData;
import modal.RJ;

public class RJListAdapter extends RecyclerView.Adapter<RJListAdapter.ViewHolder> {

    private List<RJ> rjList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    // data is passed into the constructor
    RJListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context=context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.rj_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewName.setText(rjList.get(position).getRjName());
        holder.setImage(rjList.get(position).getImage(),holder.imageViewRj,context);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return rjList.size();
    }

    public void setData(List<RJ> data) {
        rjList = data;
        notifyDataSetChanged();
    }
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewName;
        ImageView imageViewRj;


        ViewHolder(View itemView) {
            super(itemView);
            imageViewRj = itemView.findViewById(R.id.image_rj);
            textViewName = itemView.findViewById(R.id.text_rj_name);
            itemView.setOnClickListener(this);
        }
        public void setImage(String imgUrl, ImageView imageView, Context context) {
            Picasso.with(context).load(imgUrl.replace(" ", "%20"))
                    .fit()
                    .into(imageView);
        }
        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
