package com.jago.app.rj_program

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jago.app.network.APIFactory
import com.jago.app.repositories.RJRepo
import com.jago.app.repositories.VideoRepo
import com.jago.app.util.InternetConnectivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import modal.Program
import modal.RJ
import kotlin.coroutines.CoroutineContext

class RjProgramViewModel(application: Application) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Rj Fragment"
    }
    val text: LiveData<String> = _text
    private val parentJob = Job()
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)
    private val rjRepo : RJRepo = RJRepo(APIFactory.getRJApi(),APIFactory.getProgramApi())
    val rjLiveData = MutableLiveData<MutableList<RJ>>()
    private lateinit var rjList:MutableList<RJ>
    private lateinit var programList:MutableList<Program>
    val programLiveData = MutableLiveData<MutableList<Program>>()
    fun  getRJ(){
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
                rjList= rjRepo.getRJList()!!
                rjLiveData.postValue(rjList)

            }
        }
    }
    fun  getProgram(rjId:String){
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
                programList= rjRepo.getProgramList(rjId)!!
                programLiveData.postValue(programList)

            }
        }
    }
}