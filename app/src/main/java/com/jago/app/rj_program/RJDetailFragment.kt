package com.jago.app.rj_program

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jago.app.HomeActivity
import com.jago.app.R
import com.jago.app.databinding.FragmentRjDetailBinding
import com.jago.app.util.SaveToSharedPrefences
import com.squareup.picasso.Picasso
import modal.Program
import modal.RJ


class RJDetailFragment:Fragment() {

    private lateinit var fragmentRjDetailBinding: FragmentRjDetailBinding
    private lateinit var imageViewRj:ImageView
    private lateinit var textViewRjDescription: TextView
    private lateinit var recyclerView:RecyclerView
    private lateinit var textViewProgram: TextView
    private lateinit var imageViewRjFav: ImageView
    private lateinit var rjProgramViewModel: RjProgramViewModel
    private  var rj:RJ?=null
    private lateinit var programList:List<Program>
    var isFav: Boolean =false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentRjDetailBinding=  DataBindingUtil.inflate(inflater, R.layout.fragment_rj_detail,container,false)
        rj= arguments?.getParcelable<RJ>("rj")
        (activity as HomeActivity).getSupportActionBar()?.setTitle(rj?.rjName)
        val toolbar =  (activity as HomeActivity)!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.findViewById<ImageView>(R.id.toolbarImage).visibility=View.GONE
        return fragmentRjDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        imageViewRj = view.findViewById(R.id.image_view_rj_detail)
        textViewRjDescription = view.findViewById(R.id.textViewDescriptionRJ)
        recyclerView = view.findViewById(R.id.recyclerView_programs)
        textViewProgram = view.findViewById(R.id.textViewProgram)
        imageViewRjFav = view.findViewById(R.id.imageViewRjFav)
        rj?.image?.let { setImage(it,imageViewRj,activity) }
        textViewRjDescription.text =  rj?.description
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        val spanCount = 2// 3 columns

        val spacing = 20 // 50px

        val includeEdge = true
        recyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
        if(SaveToSharedPrefences.isFavIDSaved(rj?.id)){
            isFav=true
            setFavIcon()
        }else{
            isFav=false
            setFavIcon()
        }
        imageViewRjFav.setOnClickListener{
        if(isFav){
            SaveToSharedPrefences.clearSavedFavID(rj?.id)
            isFav=false
            setFavIcon()

        }else{
            SaveToSharedPrefences.saveFavId(rj?.id,"RJ")
            isFav=true
            setFavIcon()
        }
        }
    }
    fun setImage(imgUrl: String, imageView: ImageView?, context: Context?) {
        Picasso.with(context).load(imgUrl.replace(" ", "%20"))
                .fit()
                .into(imageView)
    }

    fun setFavIcon(){
        if(isFav){
            imageViewRjFav.setImageDrawable(activity?.let { ContextCompat.getDrawable(it,R.drawable.favorite_fill) })
        }else{
            imageViewRjFav.setImageDrawable(activity?.let { ContextCompat.getDrawable(it,R.drawable.favorite) })

        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rjProgramViewModel = activity?.let { ViewModelProviders.of(it).get(RjProgramViewModel::class.java) }!!
        rj?.id?.let { rjProgramViewModel.getProgram(it) }
        rjProgramViewModel.programLiveData.observe(viewLifecycleOwner, Observer {
            setUpProgramRecyclerView(it)
        })
    }
    private fun setUpProgramRecyclerView(programList: List<Program>) {
        this.programList=programList
        val rjListAdapter = activity?.let { ProgramsAdapter(activity, programList) }
        recyclerView.adapter = rjListAdapter

        textViewProgram.visibility = if (null == programList || programList.isEmpty()) View.GONE else View.VISIBLE

    }
}