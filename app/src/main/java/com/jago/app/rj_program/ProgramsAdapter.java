package com.jago.app.rj_program;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jago.app.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import modal.Program;
import modal.RJ;

public class ProgramsAdapter extends RecyclerView.Adapter<ProgramsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<Program> programList;
    private Context context;
    public ProgramsAdapter(Context context, List<Program> data){
        this.mInflater = LayoutInflater.from(context);
        this.programList = data;
        this.context=context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.program_item, parent, false);
        return new ProgramsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTextViewName.setText(programList.get(position).getProgramName());
        holder.setImage(programList.get(position).getImage(),holder.mImageView,context);
        String startTime=formatTime(programList.get(position).getStarttime());
        String endTime=formatTime(programList.get(position).getEndtime());
        String ampmStartTime = getAMPM(programList.get(position).getStarttime());
        String ampmEndTime = getAMPM(programList.get(position).getEndtime());
        String date=parseDate(programList.get(position).getDatetime());
        holder.mTextViewTime.setText(date + " "+ startTime +" "+ampmStartTime+"-"+
                endTime+" "+ampmEndTime);
    }
    public String formatTime(String Time){
        String[] timeArray = Time.split(":");
        String hour = timeArray[0];
        int hr = Integer.parseInt(hour);
        if(hr>12){
            hr= hr-12;
        }
        String time = hr+":"+ timeArray[1];
        return time;
    }
    public String getAMPM(String Time){
        String time="AM";
        String[] timeArray = Time.split(":");
        String hour = timeArray[0];
        int hr = Integer.parseInt(hour);
        if(hr>=12){
            time="PM";
        }
        return time;
    }

    public String parseDate(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd,MMM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    @Override
    public int getItemCount() {
        return programList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mTextViewName;
        private TextView mTextViewTime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.programImage);
            mTextViewName = itemView.findViewById(R.id.textviewName);
            mTextViewTime = itemView.findViewById(R.id.textviewTime);
        }
        public void setImage(String imgUrl, ImageView imageView, Context context) {
            Picasso.with(context).load(imgUrl.replace(" ", "%20"))
                    .fit()
                    .into(imageView);
        }
    }
}
