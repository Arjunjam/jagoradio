package com.jago.app.rj_program

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jago.app.HomeActivity
import com.jago.app.R
import com.jago.app.databinding.FragmentRjProgramsBinding
import modal.RJ


class RjProgramFragment : Fragment() ,RJListAdapter.ItemClickListener{

    private lateinit var rjProgramViewModel: RjProgramViewModel
    private lateinit var fragmentRjProgramsBinding :FragmentRjProgramsBinding
    private lateinit var  recyclerView: RecyclerView
    private lateinit var mEmptyView: View
    private lateinit var mLoadingView: View
    private lateinit var rjList:List<RJ>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rjProgramViewModel =
                ViewModelProviders.of(this).get(RjProgramViewModel::class.java)
        fragmentRjProgramsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_rj_programs,container,false)
        val root = inflater.inflate(R.layout.fragment_rj_programs, container, false)
//        val textView: TextView = root.findViewById(R.id.text_rj_program)
//        rjProgramViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })
        val toolbar =  (activity as HomeActivity)!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.findViewById<ImageView>(R.id.toolbarImage).visibility=View.GONE
        return fragmentRjProgramsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = fragmentRjProgramsBinding.recyclerViewRj
        mLoadingView = fragmentRjProgramsBinding.progressIndicator
        mEmptyView = fragmentRjProgramsBinding.emptyView
        val spanCount = 2
        val spacing = 30 // 50px
        val includeEdge = true
        recyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rjProgramViewModel = activity?.let { ViewModelProviders.of(it).get(RjProgramViewModel::class.java) }!!
        rjProgramViewModel.getRJ()
        rjProgramViewModel.rjLiveData.removeObservers(this);
        rjProgramViewModel.rjLiveData.observe(viewLifecycleOwner, Observer {
            setUpRJRecyclerView(it)
        })
    }
    private fun setUpRJRecyclerView(rjList: List<RJ>) {
        this.rjList=rjList
        val rjListAdapter = activity?.let { RJListAdapter(activity) }
        recyclerView.adapter = rjListAdapter
        rjListAdapter?.setClickListener(this);
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        rjListAdapter?.setData(rjList)
        mLoadingView.visibility = View.GONE
        mEmptyView.visibility = if (null == rjList || rjList.isEmpty()) View.VISIBLE else View.GONE
    }

    override fun onItemClick(view: View?, position: Int) {
        Navigation.createNavigateOnClickListener(R.id.action_fragment_rj_detail)
        val bundle:Bundle= Bundle()
        bundle.apply {
            putParcelable("rj",rjList.get(position))
        }
        Navigation.findNavController(view!!).navigate(R.id.action_fragment_rj_detail,bundle)
    }


}
