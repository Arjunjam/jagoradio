package com.jago.app.repositories

import com.jago.app.network.ProgramServiceAPI
import com.jago.app.network.RJServiceApi
import com.jago.app.network.VideoServiceApi
import com.jam.appyhigh.modal.repositories.BaseRepository
import modal.*

class RJRepo (private val apiInterface: RJServiceApi,private val apiProgramInterface: ProgramServiceAPI) : BaseRepository(){

    suspend fun getRJList() :  MutableList<RJ>?{
        return safeApiCall(
                call = {apiInterface.fetchRJAsync().await()},
                error = "Error from server" +
                        ""
        )?.rjList?.toMutableList()
    }
    suspend fun getProgramList(rjId:String) :  MutableList<Program>?{
        return safeApiCall(
                call = {apiProgramInterface.fetchProgram(rjId).await()},
                error = "Error from server" +
                        ""
        )?.programList?.toMutableList()
    }
}