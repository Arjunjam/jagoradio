package com.jago.app.repositories

import com.jago.app.network.HomeServiceApi
import com.jago.app.network.VideoServiceApi
import com.jam.appyhigh.modal.repositories.BaseRepository
import modal.HomeData

class HomeRepo(private val apiInterface: HomeServiceApi) : BaseRepository(){

    suspend fun getHomeData() :  HomeData?{
        return safeApiCall(
                call = {apiInterface.fetchHomeAsync().await()},
                error = "Error from server" +
                        ""
        )?.homeData
    }


}