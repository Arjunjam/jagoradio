package com.jago.app.repositories

import com.jago.app.network.LoginServiceAPI
import com.jago.app.network.ProgramServiceAPI
import com.jago.app.network.RJServiceApi
import com.jam.appyhigh.modal.repositories.BaseRepository
import modal.LoginResponse
import modal.Program
import modal.RJ

class LoginRepo(private val apiInterface: LoginServiceAPI) : BaseRepository() {

    suspend fun postLogin(email: String, profile: String, firstName: String?, lastName: String?, image: String?,
                          platform: String): LoginResponse? {
        return safeApiCall(
                call = { apiInterface.postLogin(email, profile, firstName, lastName, image, platform).await() },
                error = "Error from server" +
                        ""
        )

    }

}