package com.jago.app.repositories

import com.jago.app.network.VideoServiceApi
import com.jam.appyhigh.modal.repositories.BaseRepository
import modal.Video
import modal.VideoCategory

import modal.VideoResponse

class VideoRepo(private val apiInterface: VideoServiceApi) : BaseRepository(){

    suspend fun getVideo() :  MutableList<Video>?{
        return safeApiCall(
                call = {apiInterface.fetchVideoAsync().await()},
                error = "Error from server" +
                        ""
        )?.videos?.toMutableList()
    }

    suspend fun getVideoCategory() :  MutableList<VideoCategory>?{
        return safeApiCall(
                call = {apiInterface.fetchVideoCategory().await()},
                error = "Error from server" +
                        ""
        )?.videoCategories?.toMutableList()
    }
}