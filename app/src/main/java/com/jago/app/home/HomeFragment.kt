package com.jago.app.home

import android.content.Intent
import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jago.app.HomeActivity
import com.jago.app.R
import com.jago.app.mediaPlayer.LocalPlayerActivity
import com.jago.app.util.UI
import es.claucookie.miniequalizerlibrary.EqualizerView
import modal.HomeRowData
import modal.RJ
import modal.Video


class HomeFragment : Fragment(), HomeListAdapter.ItemClickListener {

    interface  HomeCallback{
        fun playMusic()
        fun stopMusic()
    }
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var mEmptyView: View
    private lateinit var mLoadingView: View
    private lateinit var mPlay: Button
    private lateinit var mFrameLayout: FrameLayout

    private var homeRowDataList: List<HomeRowData> = ArrayList<HomeRowData>()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        recyclerView = root.findViewById(R.id.recyclerView)
        mLoadingView = root.findViewById(R.id.progress_indicator)
        mEmptyView = root.findViewById(R.id.empty_view)
        mPlay = root.findViewById(R.id.playButton)
        mFrameLayout = root.findViewById(R.id.frame_layout)
      //  mFrameLayout.setBackground(activity?.let { UI.getColorOverlay(it,R.drawable.homeimage,ContextCompat.getColor(requireContext(), R.color.colorPrimary)) })
        homeViewModel.getHomeData()
        homeViewModel.observeData().observe(viewLifecycleOwner, Observer {
            setUpRecyclerView(it)
        })
        val toolbar =  (activity as HomeActivity)!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.findViewById<ImageView>(R.id.toolbarImage).visibility=View.VISIBLE
        return root
    }


    private fun setUpRecyclerView(homeRowData: List<HomeRowData>) {
        val videoRecyclerViewAdapter = activity?.let { HomeListAdapter(this, activity) }
        recyclerView.adapter = videoRecyclerViewAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        homeRowDataList = emptyList()
        this.homeRowDataList = homeRowData
        videoRecyclerViewAdapter?.setData(homeRowDataList)
        mLoadingView.visibility = View.GONE
        mEmptyView.visibility = if (null == homeRowDataList || homeRowDataList.isEmpty()) View.VISIBLE else View.GONE
    }

    override fun onResume() {
        super.onResume()
        if( recyclerView.adapter!=null){
            recyclerView.adapter!!.notifyDataSetChanged()
        }
    }
    override fun itemClicked(v: View?, item: HomeRowData?, position: Int, type: String) {

        if (type.contains("video")) {
            (activity as HomeActivity?)?.stopRadio()
            val intent = Intent(activity, LocalPlayerActivity::class.java)
            val video: Video = v?.getTag() as Video
            intent.putExtra("media", video)
            intent.putExtra("shouldStart", false)
            ActivityCompat.startActivity(requireActivity(), intent, null)
        } else {
            Navigation.createNavigateOnClickListener(R.id.action_fragment_rj_detail)
            val rj: RJ = v?.getTag() as RJ
            val bundle: Bundle = Bundle()
            bundle.apply {
                putParcelable("rj", rj)
            }
            Navigation.findNavController(view!!).navigate(R.id.action_fragment_rj_detail, bundle)
        }

    }

    override fun openNextListFragment(title: String?, position: Int) {
        if (title?.contains("Top Videos")!!) {
            Navigation.createNavigateOnClickListener(R.id.action_vide)

            Navigation.findNavController(view!!).navigate(R.id.action_vide)
        } else {
            Navigation.createNavigateOnClickListener(R.id.action_rj)

            Navigation.findNavController(view!!).navigate(R.id.action_rj)
        }

    }


}
