package com.jago.app.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jago.app.network.APIFactory
import com.jago.app.repositories.HomeRepo
import com.jago.app.repositories.RJRepo
import com.jago.app.repositories.VideoRepo
import com.jago.app.util.InternetConnectivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import modal.*
import kotlin.coroutines.CoroutineContext

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
    private val parentJob = Job()
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    private val scope = CoroutineScope(coroutineContext)
    private val homeRepository : HomeRepo = HomeRepo(APIFactory.getHomeApi())



    private  var HomeRowDataArrayList:MutableList<HomeRowData> = ArrayList()
    var  HomeRowDataDataList :MutableLiveData<MutableList<HomeRowData>> = MutableLiveData<MutableList<HomeRowData>>()

    fun getHomeData() {
        if(InternetConnectivity.isConnected(getApplication())) {
            scope.launch {
               HomeRowDataArrayList.clear()
                var homeData :HomeData?=homeRepository.getHomeData()
                var homeRowData = homeData?.videos?.let { HomeRowData("Top Videos", it,null) }
                if (homeRowData != null) {
                    HomeRowDataArrayList.add(homeRowData)
                }
                var homeRowData1 = homeData?.rjs?.let { HomeRowData("Top RJ",null, it) }
                if (homeRowData1 != null) {
                    HomeRowDataArrayList.add(homeRowData1)
                }
                HomeRowDataDataList.postValue(HomeRowDataArrayList)

            }
        }
    }

    fun observeData():MutableLiveData<MutableList<HomeRowData>>{
         return HomeRowDataDataList
    }
}