package com.jago.app.home;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jago.app.JAGOApplication;
import com.jago.app.R;

import com.jago.app.util.SaveToSharedPrefences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


import modal.HomeRowData;
import modal.RJ;
import modal.Row;
import modal.Video;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {

    private final HomeListAdapter.ItemClickListener mClickListener;
    private final Context mAppContext;
    private List<HomeRowData> homeRowDataList = new ArrayList<>();

    public HomeListAdapter(HomeListAdapter.ItemClickListener clickListener, Context context) {
        mClickListener = clickListener;
        mAppContext = context.getApplicationContext();
    }

    @Override
    public HomeListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        View parent = LayoutInflater.from(context).inflate(R.layout.home_cell, viewGroup, false);
        return HomeListAdapter.ViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(HomeListAdapter.ViewHolder viewHolder, final int position) {
        final HomeRowData item = homeRowDataList.get(position);
        viewHolder.mParentCategoryText.setText(item.getTitle());
        List<Video> videos = null;
        List<RJ> rjs = null;
        if (item.getTitle().contains("Top Videos")) {
            videos = item.getVideos();
        } else if (item.getTitle().contains("Top RJ")) {
            rjs = item.getRjs();
        }
        if (videos != null) {
            if (videos.size() > 0) {
                viewHolder.mImageView1stItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView1stItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView1stItem.setText(videos.get(0).getVideoName());
                viewHolder.setImage(videos.get(0).getImage(), viewHolder.mImageView1stItem, mAppContext);
                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(videos.get(0).getId())){
                    setFavIcon(viewHolder.mImageView1stFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView1stFav,false);
                }
            }
            if (videos.size() > 1) {
                viewHolder.mImageView2ndItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView2ndItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView2ndItem.setText(item.getVideos().get(1).getVideoName());
                viewHolder.setImage(item.getVideos().get(1).getImage(), viewHolder.mImageView2ndItem, mAppContext);
                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(videos.get(1).getId())){
                    setFavIcon(viewHolder.mImageView2ndFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView2ndFav,false);
                }
            } else {
                viewHolder.mImageView2ndItem.setVisibility(View.GONE);
                viewHolder.mTextView2ndItem.setVisibility(View.GONE);
                viewHolder.mImageView3rdItem.setVisibility(View.GONE);
                viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (videos.size() > 2) {
                viewHolder.mImageView3rdItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView3rdItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView3rdItem.setText(item.getVideos().get(2).getVideoName());
                viewHolder.setImage(item.getVideos().get(2).getImage(), viewHolder.mImageView3rdItem, mAppContext);
                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(videos.get(2).getId())){
                    setFavIcon(viewHolder.mImageView3rdFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView3rdFav,false);
                }
            } else {
                viewHolder.mImageView3rdItem.setVisibility(View.GONE);
                viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (videos.size() > 3 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewHolder.mImageView4thItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView4thItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView4thItem.setText(item.getVideos().get(3).getVideoName());
                viewHolder.setImage(item.getVideos().get(3).getImage(), viewHolder.mImageView4thItem, mAppContext);
            } else {
//            viewHolder.mImageView3rdItem.setVisibility(View.GONE);
//            viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (videos.size() > 3 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                viewHolder.mTextViewSeeAll.setVisibility(View.VISIBLE);
            } else if (videos.size() > 4 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewHolder.mTextViewSeeAll.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mTextViewSeeAll.setVisibility(View.GONE);
            }

            List<Video> finalVideos = videos;
            viewHolder.mImageView1stItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalVideos.get(0));
                    mClickListener.itemClicked(v, item, position,"video");
                }
            });

            viewHolder.mImageView2ndItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalVideos.get(1));
                    mClickListener.itemClicked(v, item, position,"video");
                }
            });
            viewHolder.mImageView3rdItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalVideos.get(2));
                    mClickListener.itemClicked(v, item, position,"video");
                }
            });
            if (viewHolder.mImageView4thItem != null) {
                viewHolder.mImageView4thItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setTag(finalVideos.get(3));
                        mClickListener.itemClicked(v, item, position,"video");
                    }
                });
            }

            viewHolder.mTextViewSeeAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(item.getTitle());
                    mClickListener.openNextListFragment(item.getTitle(), position);
                }
            });

            viewHolder.mImageView1stFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalVideos.get(0).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView1stFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"VIDEO");
                        setFavIcon( viewHolder.mImageView1stFav,true);
                    }
                }
            });
            viewHolder.mImageView2ndFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalVideos.get(1).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView2ndFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"VIDEO");
                        setFavIcon( viewHolder.mImageView2ndFav,true);
                    }
                }
            });

            viewHolder.mImageView3rdFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalVideos.get(2).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView3rdFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"VIDEO");
                        setFavIcon( viewHolder.mImageView3rdFav,true);
                    }
                }
            });

        } else if (rjs != null) {
            if (rjs.size() > 0) {
                viewHolder.mImageView1stItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView1stItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView1stItem.setText(rjs.get(0).getRjName());
                viewHolder.setImage(rjs.get(0).getImage(), viewHolder.mImageView1stItem, mAppContext);

                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(rjs.get(0).getId())){
                    setFavIcon(viewHolder.mImageView1stFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView1stFav,false);
                }

            }
            if (rjs.size() > 1) {
                viewHolder.mImageView2ndItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView2ndItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView2ndItem.setText(rjs.get(1).getRjName());
                viewHolder.setImage(rjs.get(1).getImage(), viewHolder.mImageView2ndItem, mAppContext);
                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(rjs.get(1).getId())){
                    setFavIcon(viewHolder.mImageView2ndFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView2ndFav,false);
                }
            } else {
                viewHolder.mImageView2ndItem.setVisibility(View.GONE);
                viewHolder.mTextView2ndItem.setVisibility(View.GONE);
                viewHolder.mImageView3rdItem.setVisibility(View.GONE);
                viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (rjs.size() > 2) {
                viewHolder.mImageView3rdItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView3rdItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView3rdItem.setText(rjs.get(2).getRjName());
                viewHolder.setImage(rjs.get(1).getImage(), viewHolder.mImageView3rdItem, mAppContext);
                if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(rjs.get(2).getId())){
                    setFavIcon(viewHolder.mImageView3rdFav,true);
                }else{
                    setFavIcon(viewHolder.mImageView3rdFav,false);
                }
            } else {
                viewHolder.mImageView3rdItem.setVisibility(View.GONE);
                viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (rjs.size() > 3 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewHolder.mImageView4thItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView4thItem.setVisibility(View.VISIBLE);
                viewHolder.mTextView4thItem.setText(rjs.get(3).getRjName());
                viewHolder.setImage(rjs.get(3).getImage(), viewHolder.mImageView4thItem, mAppContext);
            } else {
//            viewHolder.mImageView3rdItem.setVisibility(View.GONE);
//            viewHolder.mTextView3rdItem.setVisibility(View.GONE);
            }
            if (rjs.size() > 3 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                viewHolder.mTextViewSeeAll.setVisibility(View.VISIBLE);
            } else if (rjs.size() > 4 && mAppContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewHolder.mTextViewSeeAll.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mTextViewSeeAll.setVisibility(View.GONE);
            }

            List<RJ> finalRjs = rjs;
            viewHolder.mImageView1stItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalRjs.get(0));
                    mClickListener.itemClicked(v, item, position,"rj");
                }
            });
            viewHolder.mImageView2ndItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalRjs.get(1));
                    mClickListener.itemClicked(v, item, position,"rj");
                }
            });
            viewHolder.mImageView3rdItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(finalRjs.get(2));
                    mClickListener.itemClicked(v, item, position,"rj");
                }
            });
            if (viewHolder.mImageView4thItem != null) {
                viewHolder.mImageView4thItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setTag(finalRjs.get(3));
                        mClickListener.itemClicked(v, item, position,"rj");
                    }
                });
            }

            viewHolder.mTextViewSeeAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(item.getTitle());
                    mClickListener.openNextListFragment(item.getTitle(), position);
                }
            });
            viewHolder.mImageView1stFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalRjs.get(0).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView1stFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"RJ");
                        setFavIcon( viewHolder.mImageView1stFav,true);
                    }
                }
            });
            viewHolder.mImageView2ndFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalRjs.get(1).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView2ndFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"RJ");
                        setFavIcon( viewHolder.mImageView2ndFav,true);
                    }
                }
            });
            viewHolder.mImageView3rdFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isFav = false;
                    String id=finalRjs.get(2).getId();
                    if(SaveToSharedPrefences.saveDataLocally.isFavIDSaved(id)){
                        isFav=true;
                    }
                    if(isFav){
                        SaveToSharedPrefences.saveDataLocally.clearSavedFavID(id);
                        setFavIcon( viewHolder.mImageView3rdFav,false);

                    }else{
                        SaveToSharedPrefences.saveDataLocally.saveFavId(id,"RJ");
                        setFavIcon( viewHolder.mImageView3rdFav,true);
                    }
                }
            });
        }

//

//


////


//
//

//
//        viewHolder.mTextContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mClickListener.itemClicked(v, item, position);
//            }
//        });
    }

    public void setFavIcon(ImageView imageView, boolean isFav){
        if(isFav){
            imageView.setImageDrawable( ContextCompat.getDrawable(JAGOApplication.instance,R.drawable.favorite_fill));
        }else{
            imageView.setImageDrawable(ContextCompat.getDrawable(JAGOApplication.instance,R.drawable.favorite));

        }
    }
    @Override
    public int getItemCount() {
        return homeRowDataList == null ? 0 : homeRowDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final View mParent;


        private TextView mParentCategoryText;

        private ImageView mImageView1stItem;
        private ImageView mImageView2ndItem;
        private ImageView mImageView3rdItem;
        private ImageView mImageView4thItem;

        private TextView mTextView1stItem;
        private TextView mTextView2ndItem;
        private TextView mTextView3rdItem;
        private TextView mTextView4thItem;

        private TextView mTextViewSeeAll;

        private ImageView mImageView1stFav;
        private ImageView mImageView2ndFav;;
        private ImageView mImageView3rdFav;;


        public static HomeListAdapter.ViewHolder newInstance(View parent) {
            TextView mParentCategoryText = parent.findViewById(R.id.textView_heading);

            TextView mTextView1stItem = parent.findViewById(R.id.textView_title1);
            TextView mTextView2ndItem = parent.findViewById(R.id.textView_title2);
            TextView mTextView3rdItem = parent.findViewById(R.id.textView_title3);
            TextView mTextView4thItem = parent.findViewById(R.id.textView_title4);
            TextView mTextViewSeeAll = parent.findViewById(R.id.textView_see_all);

            ImageView mImageView1stItem = parent.findViewById(R.id.imageView1);
            ImageView mImageView2ndItem = parent.findViewById(R.id.imageView2);
            ImageView mImageView3rdItem = parent.findViewById(R.id.imageView3);
            ImageView mImageView4thItem = parent.findViewById(R.id.imageView4);

            ImageView mImageView1stFav = parent.findViewById(R.id.imageViewFav1);
            ImageView mImageView2ndFav = parent.findViewById(R.id.imageViewFav2);
            ImageView mImageView3rdFav = parent.findViewById(R.id.imageViewFav3);


            return new HomeListAdapter.ViewHolder(parent, mParentCategoryText, mImageView1stItem,
                    mImageView2ndItem, mImageView3rdItem, mImageView4thItem, mTextView1stItem, mTextView2ndItem, mTextView3rdItem, mTextView4thItem, mTextViewSeeAll,
                    mImageView1stFav,mImageView2ndFav,mImageView3rdFav);
        }

        private ViewHolder(View parent, TextView parentCategory, ImageView imageView1stItem, ImageView imageView2ndItem,
                           ImageView imageView3rdItem, ImageView imageView4thItem, TextView textView1stItem, TextView textView2ndItem, TextView textView3rdItem,
                           TextView textView4thItem, TextView textViewSeeAll,ImageView imageView1stFav,ImageView imageView2ndFav,ImageView imageView3rdFav) {
            super(parent);
            mParent = parent;
            mParentCategoryText = parentCategory;

            mImageView1stItem = imageView1stItem;
            mImageView2ndItem = imageView2ndItem;
            mImageView3rdItem = imageView3rdItem;
            mImageView4thItem = imageView4thItem;

            mTextView1stItem = textView1stItem;
            mTextView2ndItem = textView2ndItem;
            mTextView3rdItem = textView3rdItem;
            mTextView4thItem = textView4thItem;
            mTextViewSeeAll = textViewSeeAll;

            mImageView1stFav =imageView1stFav;
            mImageView2ndFav =imageView2ndFav;
            mImageView3rdFav=imageView3rdFav;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mImageView1stItem.setClipToOutline(true);
            }
        }


        public void setImage(String imgUrl, ImageView imageView, Context context) {
            Picasso.with(context).load(imgUrl.replace(" ", "%20"))
                    .fit()
                    .into(imageView);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            mParent.setOnClickListener(listener);
        }


    }

    public void setData(List<HomeRowData> data) {
        homeRowDataList.clear();
        homeRowDataList = data;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {

        void itemClicked(View v, HomeRowData item, int position,String type);

        void openNextListFragment(String categoryId, int position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
