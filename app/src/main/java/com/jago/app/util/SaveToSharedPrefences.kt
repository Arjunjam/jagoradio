package com.jago.app.util

import android.content.Context
import android.content.SharedPreferences
import com.jago.app.JAGOApplication

class SaveToSharedPrefences {

    companion object saveDataLocally{
        val PREFRENCE_NAME="JAGOSHARED_PREFRENCES"
        val TOKEN= "TOKEN"
      fun  save(token:String,context:Context){
          val sharedPref = getSharedPrefrence(context)
          with(sharedPref.edit()){
              putString(TOKEN,token)
              apply()
          }
      }
        fun isTokenSaved(context: Context):Boolean{
            val sharedPref = getSharedPrefrence(context)
            sharedPref.let {
            val token = sharedPref.getString(TOKEN,"")
                if(!token.isNullOrEmpty()){
                    return true
                }
            }

            return false
        }

         fun clearSavedToken(context: Context){
             val sharedPref = getSharedPrefrence(context)
             with(sharedPref.edit()){
                 remove(TOKEN)
                 apply()
             }

         }
        fun getSharedPrefrence(context: Context):SharedPreferences{
            val sharedPref = context.getSharedPreferences("PREFRENCE_NAME",Context.MODE_PRIVATE)
            return sharedPref
        }

        fun  saveFavId(id:String?,value:String){
            val sharedPref = getSharedPrefrence(JAGOApplication.instance)
            with(sharedPref.edit()){
                putString(id,value)
                apply()
            }
        }

        fun clearSavedFavID(id:String?){
            val sharedPref = getSharedPrefrence(JAGOApplication.instance)
            with(sharedPref.edit()){
                remove(id)
                apply()
            }

        }

        fun isFavIDSaved(id:String?):Boolean{
            val sharedPref = getSharedPrefrence(JAGOApplication.instance)
            sharedPref.let {
                val token = sharedPref.getString(id,"")
                if(!token.isNullOrEmpty()){
                    return true
                }
            }

            return false
        }
    }
}