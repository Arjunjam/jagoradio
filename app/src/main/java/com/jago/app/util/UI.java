package com.jago.app.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

public class UI {

    public static Drawable getColorOverlay(@NonNull Context context, @DrawableRes int resourceid, @ColorInt int color) {
        try {
            Drawable drawable = context.getResources().getDrawable(resourceid);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            drawable.setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);
            drawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
            return drawable;
        } catch (Exception e) {
        }
        try {
            return context.getResources().getDrawable(resourceid);
        } catch (Exception e) {
        }
        return null;
    }

}
