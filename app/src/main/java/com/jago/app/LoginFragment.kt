package com.jago.app

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.jago.app.databinding.FragmentLoginBinding
import com.jago.app.login.LoginViewModal
import com.jago.app.util.SaveToSharedPrefences
import java.util.*


class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()

    }
    //Google Login Request Code
    private val RC_SIGN_IN = 7
    private lateinit var loginBinding:FragmentLoginBinding;
    private lateinit var loginButtonFaceBook:LoginButton
    private lateinit var callbackManager: CallbackManager
    private lateinit var auth: FirebaseAuth
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mSignButton: SignInButton
    private lateinit var mSkipButton: Button

    private lateinit var  loginViewModel:LoginViewModal
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance();
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        loginBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_login,container,false)
        (activity as MainActivity).getSupportActionBar()?.setTitle("Login")
        loginViewModel = ViewModelProvider(this).get(LoginViewModal::class.java)
        mSkipButton = loginBinding.buttonSkipLogin
        loginButtonFaceBook = loginBinding.fbLogin
        mSignButton = loginBinding.googleSignIn
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = activity?.let { GoogleSignIn.getClient(it,gso) }!!
        mSignButton.setOnClickListener { signIn() }

        callbackManager = CallbackManager.Factory.create();
        loginButtonFaceBook.setReadPermissions(Arrays.asList("EMAIL"))
        loginButtonFaceBook.setOnClickListener(View.OnClickListener {
//            LoginManager.getInstance()
//                    .logInWithReadPermissions(this, Arrays.asList( "email"))
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    // App code //API CALL
                    Log.println(Log.VERBOSE,"USERID>>>>>>",loginResult?.accessToken?.userId.toString())
                    // handleFacebookAccessToken(loginResult.accessToken.toString())
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Log.println(Log.ERROR,"USERID>>>>>>",exception.toString())
                }
            })
        })


        mSkipButton.setOnClickListener {
            openHomeActivity()
        }
        return loginBinding.root
    }

    private fun makeNetworkCallForLogin(email: String, profile: String, firstName: String?, lastName: String?, image: String?,
                                        platform: String){
        loginViewModel.postLogin(email, profile, firstName,
                lastName, image, platform)
        loginViewModel.liveDataLogin.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loginResponse-> if(loginResponse.success ==1 && loginResponse.data > 0){
            openHomeActivity()
            activity?.let { SaveToSharedPrefences.saveDataLocally.save(profile, it) }
        }
        })
    }
    private fun openHomeActivity() {
        val intent = Intent(activity, HomeActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
       // Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        activity?.let {
            auth.signInWithCredential(credential).addOnCompleteListener(it) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
//                        Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                   // updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
//                        Log.w(TAG, "signInWithCredential:failure", task.exception)
//                        Toast.makeText(baseContext, "Authentication failed.",
//                                Toast.LENGTH_SHORT).show()
                    //updateUI(null)
                }

                // ...
            }
        }
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
     override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val account = GoogleSignIn.getLastSignedInAccount(activity)
       // updateUI(currentUser)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);

        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode === RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }



    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            Log.println(Log.VERBOSE,"GMAILLL USERID>>>>>>", account?.id.toString())
            // Signed in successfully, show authenticated UI.
           // updateUI(account)
            val email = account?.email
            val token = account?.id
            val firstName = account?.givenName
            val lastName = account?.familyName
            val image = account?.photoUrl?.toString()
            if (email != null) {
                if (token != null) {
                    makeNetworkCallForLogin(email,token,firstName,lastName,image,"Gmail")
                }
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            //Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            //updateUI(null)
        }
    }
}