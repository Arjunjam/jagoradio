package com.jago.app



import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.jago.app.MusicService.ServiceBinder
import com.jago.app.util.SaveToSharedPrefences
import com.jago.app.util.UI
import es.claucookie.miniequalizerlibrary.EqualizerView


class HomeActivity  : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
     var isPlaying =false;
    private lateinit var mPlayRadio: ImageView
    private lateinit var equalizer: EqualizerView
    private lateinit var playerLayout: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
//        toolbar.visibility = View.GONE
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        //actionbar?.setHomeAsUpIndicator(R.drawable.ic_)
        actionbar?.setDisplayHomeAsUpEnabled(true)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_podcast, R.id.nav_rj_program,R.id.nav_video,R.id.nav_favorite), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        doBindService()
        val music = Intent()
        music.setClass(this, MusicService::class.java)
        startService(music)
        mPlayRadio = findViewById(R.id.play_radio)
        equalizer =  findViewById(R.id.equalizer_view)
        playerLayout=  findViewById(R.id.play_layout)

        equalizer.stopBars()
        mPlayRadio.setOnClickListener {
            if(!isPlaying) {
                playRadio()
            }else{
                stopRadio()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId==R.id.logout){
            SaveToSharedPrefences.clearSavedToken(applicationContext)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    //Bind/Unbind music service
    private var mIsBound = false
    private var mServ: MusicService? = null
    private val Scon: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            mServ = (binder as ServiceBinder).service
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mServ = null
        }
    }

    fun doBindService() {
        bindService(Intent(this, MusicService::class.java),
                Scon, Context.BIND_AUTO_CREATE)
        mIsBound = true
    }

    fun doUnbindService() {
        if (mIsBound) {
            unbindService(Scon)
            mIsBound = false
        }
    }

    override fun onResume() {
        super.onResume()
        if(!isPlaying){
            stopRadio()
        }else{
            playRadio()
        }

    }

    override fun onPause() {
        super.onPause()

    }
    fun playRadio(){
        if (mServ != null) {
            mServ?.startMusic()
        }
        isPlaying=true

        mPlayRadio.setImageDrawable(let { it1 ->
            UI.getColorOverlay(it1,
                    R.drawable.pause_ic, ContextCompat.getColor(this, R.color.colorPrimary))
        })
        equalizer.animateBars()
        equalizer.visibility = View.VISIBLE
        //playerLayout.setBackgroundColor( ContextCompat.getColor(this, R.color.black))
    }

    fun stopRadio(){
        if (mServ != null) {
            mServ?.stopMusic()
        }
        isPlaying=false


        mPlayRadio.setImageDrawable(let{ it1 ->
            UI.getColorOverlay(it1,
                    R.drawable.play_ic, ContextCompat.getColor(this, R.color.colorPrimary))
        })
        equalizer.stopBars()
        equalizer.visibility = View.GONE
       // playerLayout.setBackgroundColor( ContextCompat.getColor(this, R.color.white))
    }
    override fun onDestroy() {
        super.onDestroy()

        //UNBIND music service
        doUnbindService()
        val music = Intent()
        music.setClass(this, MusicService::class.java)
        stopService(music)
    }


}
